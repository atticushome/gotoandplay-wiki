#Introduction

Deployments are handled with Grunt.

Server details and `src/dest` folders are detailed in `grunt/ftp-deploy.js`

Server authentication credentials are detailed in `.ftppass` that is located in the project root. This file should have the following syntax: 

```
{
  "key1": {
    "username": "username1",
    "password": "password1"
  },
  "key2": {
    "username": "username2",
    "password": "password2"
  }
}
```
This file uses standard JSON syntax, so make sure to use double quotes.

The key names should be same as the server address in `grunt/ftp-deploy.js`

#How to deploy and bump versions

To deploy to a staging server, write

`grunt deploy:staging`

By default deployments to staging server do not bump the repo version. If you still want to bump the repo version with a staging deploy, write

`grunt deploy:staging:[major/minor/patch]`

To deploy to a live server, write

`grunt deploy:live`

By default deployments to liveserver bump the repo patch version number. If you want to bump the repo major/minor version number with a livedeploy, write

`grunt deploy:live:[major/minor]`

Version bumping automatically creates a commit in your local git repo with the version tag and a commit message that specifies where you deployed. You still have to manually push this to the remote repo

#Version bumping without deployment

If you want to bump the repo without deploying to a server, write

`npm version [major/minor/patch] -m "Bumped version to %s"`
