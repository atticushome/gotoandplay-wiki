#Introduction

The following info requires that you have already read the [Introduction to Grunt](https://bitbucket.org/gtap-dev/grunt-base-wordpress/wiki/Introduction%20to%20Grunt) page

## load-grunt-config ([GitHub](https://github.com/firstandthird/load-grunt-config))

We use the `load-grunt-config` Grunt plugin  that allows us to split up our Gruntfile by task. This means that all of our Grunt config is located in the `grunt` folder in the project root.

I will now walk through all of our config.

# CSS

## grunt-contrib-sass ([GitHub](https://github.com/gruntjs/grunt-contrib-sass))

We write [Sass](http://sass-lang.com/).

We tend to write our Sass as follows:

- split everything up in partials
- `@import` all the partials to `global.scss`

We use the `grunt-contrib-sass` plugin to compile our Sass files. The default config looks like this:

```
module.exports = {
	all: {
		options: {
			style: 'compressed'
		},
		files: {
			'src/css/global-unprefixed.css': 'src/css/global.scss'
		}
	}
};
```
The default output style is `compressed` because we never look at the compiled file. The compiled file remains in `src/css` since we are not done with it yet.

## grunt-autoprefixer ([GitHub](https://github.com/nDmitry/grunt-autoprefixer))

We don't like to deal with vendor prefixes manually. Thankfully, there's Autoprefixer. It uses the caniuse.com live database to determine which CSS properties need prefixing.

The default config is as follows:

```
module.exports = {
	static: {
		src: 'src/css/global-unprefixed.css',
		dest: 'app/static/inc/global.min.css'
	},
	theme: {
		src: 'src/css/global-unprefixed.css',
		dest: 'app/wordpress/wp-content/themes/theme/inc/global.min.css'
	}
};
```

The Autoprefixer plugin is given the compiled CSS file as the source. The destination depends on the target.

#JS

## grunt-import ([GitHub](https://github.com/marcinrosinski/grunt-import))

We wanted a Sass-like `@import` system to deal with many JS files. The `grunt-import` plugin gives us that option.

We tend to write our JS as follows:

- split everything up in partials
- `@import` all the partials to `global.js`ˇ

The default config looks like this:

```
module.exports = {
	static: {
		src: 'src/js/global.js',
		dest: 'app/static/inc/js/global.js',
	},
	theme: {
		src: 'src/js/global.js',
		dest: 'app/wordpress/wp-content/themes/theme/inc/js/global.js',
	}
};
```

This task takes the source file, imports and places it in the destination (depending on the target). With JS, it is good to have the not uglified version of the file on hand for debugging purposes.

## grunt-contrib-uglify ([GitHub](https://github.com/gruntjs/grunt-contrib-uglify))

The `uglify` task minifies our JS. The config is fairly simple:

```
module.exports = {
	static: {
		src: 'app/static/inc/js/global.js',
		dest: 'app/static/inc/js/global.min.js'
	},
	theme: {
		src: 'app/wordpress/wp-content/themes/theme/inc/js/global.js',
		dest: 'app/wordpress/wp-content/themes/theme/inc/js/global.min.js'
	}
};
```
The task takes the `global.js` file where everything is already imported, uglifies it and places it in the same folder, appending `.min` to the filename.

# SVG icons

We like to use [SVG sprites](http://css-tricks.com/svg-sprites-use-better-icon-fonts/) for our icon system, since it's fairly well supported, scales well and does not have the common font rendering issues that icon fonts have.

Please note: SVG icons and IE8 are not very good friends. SVG icons & IE9 have fun with the help of [svg4everybody](https://github.com/jonathantneal/svg4everybody).

## grunt-svgmin ([GitHub](https://github.com/sindresorhus/grunt-svgmin))

All of your icons exported from Illustrator go into `src/svg`. The `svgmin` task takes these icons, minifies them and puts them into `src/svg/min`. The config is as follows: 

```
module.exports = {
	options: {
		plugins: [
		{ removeViewBox: false },
		{ removeUselessStrokeAndFill: true }, //removes fill="#000"
		{ removeXMLProcInst:false }
		]
	},
	all: {
		files: [{
			expand: true,
			cwd: 'src/svg/',
			src: ['*.svg'],
			dest: 'src/svg/min/',
		}]
	}
};
```
## grunt-svgstore ([GitHub](https://github.com/FWeinb/grunt-svgstore))

The `svgstore` task takes the minified SVG files, turns them into `<symbol>`'s and makes a sprite of them. Config:

```
module.exports = {
	options: {
		prefix : 'icon-',
		svg: {
			style: 'display:none;',
			viewBox: '0 0 32 32',
			version: '1.1',
			xmlns: 'http://www.w3.org/2000/svg',
			'xmlns:xlink': 'http://www.w3.org/1999/xlink'
		},
		cleanup: true
	},
	static: {
		files: [{
			expand: true,
			cwd: 'src/svg/min/',
			src: ['*.svg'],
			dest: 'app/static/inc/global.svg',
		}],
	},
	theme: {
		files: [{
			expand: true,
			cwd: 'src/svg/min/',
			src: ['*.svg'],
			dest: 'app/wordpress/wp-content/themes/theme/inc/global.svg',
		}],
	}
};
```

It adds an ID to every `symbol` with the specified prefix, like `icon-filename`. This way you can easily use that icon like this:

```
<svg>
	<use xlink:href="path/to/global.svg#icon-filename">
</svg>
```

#TODO

Minifying image assets, dealing with fonts, deploy link, grunt-watch, build, aliases.
