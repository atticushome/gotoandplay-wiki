Grunt and Grunt plugins are installed and managed via [npm](https://npmjs.org/), the Node.js package manager. Make sure your [npm](https://npmjs.org/) is up-to-date by running `npm update -g npm` (this might require `sudo` on certain systems).

In order to get started, you'll want to install Grunt's command line interface (CLI) globally. [Read more about installing the CLI and How the CLI works](http://gruntjs.com/getting-started).

Before you can start working with (new) project, run following command under corresponding directory
```
npm install
grunt build:[static/theme]
```
The first line will install all the Grunt dependencies the project has and the last will build the src state. That's really all there is to it. Installed Grunt tasks can be listed by following commands:

To develop a static HTML site, run

* `grunt static`

This builds from your src folder to app/static, starts a localhost server and starts watching your source files.
The target option is optional in this case, since `static` is the default value.

To develop a WordPress theme, run

* `grunt theme`

This builds from your src folder to app/theme and starts watching your source files.
