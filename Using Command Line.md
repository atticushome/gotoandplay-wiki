#3 necessary guidelines to keep in mind!

## Starting new project repository based on grunt-base (shallow clone)

1) Create a new project folder in your computer. This will be the place for local repository.
```
mkdir the_project_name
```

2) Create a new repository in Bitbucket. This will be the place for remote repository. We will use the clone url in step 4;

3) Go to created directory `cd the_project_name`. Clone the **Base Grunt** repository. Run following command under corresponding folder (the dot in the end will clone it to current destination):
```
git clone the_clone_url .
```
You can get the_clone_url from sidebar actions. Mind the optional `--depth 1` attribute, when you take this repo for your project base, you don't need to preserve all the grunt-base commits. This is called shallow clone.

4) We need to change the origin url to new one (since we are creating a new one): 
```
git remote set-url origin the_url_of_your_repo_here
```
You can get *the_url_of_your_repo_here* when you have created the corresponding repository in Bitbucket.

5) You are almost done. Just need to edit package.json according to your project - change the **name** and **repository url**.

6) You are all set!

---

## Starting with excisting project repository

1) Create a new project folder in your computer. This will be the place for local repository;

2) Clone the project repository to previously created folder. Run following command under corresponding folder:
```
git clone the_clone_url .
```
3) You are all set!

---

## Project flow: fetch-pull-add-commit

### Checking the Status
Before you start, type the git status command to see what the current state of our project is `git status`

###Fetch & pull

**Case 1: Don't care about local repo changes**
```
git fetch origin
git reset --hard HEAD
git pull
```

**Case 2: You care about local changes**
```
git fetch origin
git status
```
If it reports something like *Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.* In that case, get the latest version by `git pull`. Otherwise you need to merge or otherwise resolve the conflict(s)

### Adding and commiting
To tell Git to start tracking changes made to file(s), we first need to add it to the staging area by using git add.
```
git add the_file_name
```
Or use `git add .` to add all untracked files. You can unstage files by using the git reset command, e.g. `git reset the_file_name`.

To store staged changes we run the commit command with a message describing what we've changed.
```
git commit -m "Add cute message describing your task"
```

###Push to Bitbucket
The push command tells Git where to put our commits when we're ready, and boy we're ready. So let's push our local changes to our origin repo (on Bitbucket).

The name of our remote is origin and the default local branch name is master. The -u tells Git to remember the parameters, so that next time we can simply run git push and Git will know what to do. Go ahead and push it!
```
git push -u origin master
```